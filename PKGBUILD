# Maintainer: Antoni Aloy Torrens <aaloytorrens@gmail.com>

# From Arch Linux AUR:
# Co-Maintainer: beliys <aur at bil dot co dot ua>
# based on lens-bin
pkgname=openlens
# Check that new versions are not buggy before updating: https://aur.archlinux.org/packages/openlens-bin
pkgver=6.2.5
pkgrel=0
pkgdesc='The Kubernetes IDE (Lens fork without lenscloud-lens-extension)'
arch=('amd64')
license=('MIT')
url='https://k8slens.dev'
depends=('libxss1' 'libnss3' 'libgtk-3-0')
source=(
	"openlens.desktop"
	"${pkgname}-${pkgver}-x86_64.AppImage::https://github.com/beliys/OpenLens/releases/download/v${pkgver}/${pkgname}-${pkgver}.x86_64.AppImage"
)
sha256sums=('30ab2e9f91ca6be993b5893fb385a225c6d06f6d11caa305e0d109348b5d132c' '7775eb8f57719264ed0014823d01a2eda1ea29fd7c1f0a616f86b0b8e0af41c8')

prepare() {
	chmod +x "${pkgname}-${pkgver}-${CARCH}.AppImage"
	"./${pkgname}-${pkgver}-${CARCH}.AppImage" --appimage-extract
}

package() {
	# move the entire distribution to /usr/share
	mkdir -p "${pkgdir}"/usr/share/${pkgname}
	mv "${srcdir}"/squashfs-root/* \
	"${pkgdir}"/usr/share/${pkgname}

	# icon
	install -Dm 644 "${pkgdir}"/usr/share/${pkgname}/usr/share/icons/hicolor/512x512/apps/open-lens.png \
	"${pkgdir}"/usr/share/icons/hicolor/512x512/apps/open-lens.png

	# desktop file
	install -Dm 644 "${srcdir}"/${pkgname}.desktop \
	"${pkgdir}"/usr/share/applications/${pkgname}.desktop

	# symlink binary
	mkdir -p "${pkgdir}"/usr/bin
	ln -sf /usr/share/${pkgname}/open-lens \
	"${pkgdir}"/usr/bin/open-lens

	# Another symlink without the dash
	ln -sf /usr/share/${pkgname}/open-lens \
	"${pkgdir}"/usr/bin/openlens

	# clean and fix permissions
	find "${pkgdir}" -type d -exec chmod 755 {} \;
	chmod -x "${pkgdir}"/usr/share/${pkgname}/*.so
	rm -rf "${pkgdir}"/usr/share/${pkgname}/AppRun
	rm -rf "${pkgdir}"/usr/share/${pkgname}/lens.{desktop,png}
	rm -rf "${pkgdir}"/usr/share/${pkgname}/usr
	rm -rf "${pkgdir}"/usr/share/${pkgname}/resources/extensions/*/dist/*-arm64
}
